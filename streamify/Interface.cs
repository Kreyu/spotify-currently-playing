﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace streamify
{
    class Interface
    {
        public static void writeCurrentSong(string songName)
        {
            Interface.ClearLine();
            Console.Write(songName);
        }

        public static void ClearLine()
        {
            int top = Console.CursorTop - 1;

            if (top < 0)
            {
                top = 0;
            }

            Console.SetCursorPosition(0, Console.CursorTop);
            Console.Write(new string(' ', Console.WindowWidth));
            Console.SetCursorPosition(0, top);
        }
    }
}
