﻿using System;
using System.Diagnostics;
using System.IO;
using System.Timers;

namespace streamify
{
    class Spotify
    {
        public const string processName = "Spotify";
        public const string notPlayingInfo = "Song not playing";

        private DateTime lastFetch;
        private Timer timer;

        private string currentSong = null;
        private string lastSong = null;

        public Spotify()
        {
            this.timer = this.CreateTimer();
        }

        public string GetWindowTitle()
        {
            Process[] processList = Process.GetProcessesByName(Spotify.processName);

            foreach (Process process in processList)
            {
                string windowTitle = process.MainWindowTitle;

                if (!String.IsNullOrEmpty(windowTitle))
                {
                    return windowTitle;
                }
            }

            return null;
        }

        public string GetCurrentSong()
        {
            this.lastFetch = DateTime.Now;
            this.lastSong = this.currentSong;

            this.currentSong = this.GetWindowTitle() 
                ?? Spotify.notPlayingInfo;

            return this.currentSong;
        }

        public void StartWriting()
        {
            this.timer.Enabled = true;
        }

        private Timer CreateTimer(int interval = 1000)
        {
            timer = new Timer(interval);
            timer.Elapsed += new ElapsedEventHandler(WriteCurrentSong);

            return timer;
        }

        private void WriteCurrentSong(object source, ElapsedEventArgs e)
        {
            string path = "D:\\test.txt";

            if (this.currentSong != this.lastSong)
            {
                Interface.write("Current song: " + this.currentSong);
            }

            
            File.WriteAllText(path, this.GetCurrentSong());
        }
    }
}
