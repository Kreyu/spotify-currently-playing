﻿using System;

namespace streamify
{
    class Program
    {
        private readonly Spotify spotify;

        public static void Main(string[] args)
        {
            Spotify spotify = new Spotify();

            spotify.StartWriting();

            Console.ReadLine();
        }
    }
}
